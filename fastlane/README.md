fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew cask install fastlane`

# Available Actions
### certificates
```
fastlane certificates
```
Installs the certificates and profiles locally

----

## iOS
### ios example
```
fastlane ios example
```

### ios configureTeam
```
fastlane ios configureTeam
```

### ios appstore
```
fastlane ios appstore
```
Submit a new build to iTunes Connect

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
